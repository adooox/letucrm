<?php
/**
 * Created by PhpStorm.
 * User: lenvji
 * Date: 2018/11/5
 * Time: 14:53
 */

namespace app\home\controller;


use think\Controller;

class Base extends Controller {
    public function __construct(){
        parent::__construct();
    }
}